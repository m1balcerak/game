#!/bin/ksh
# This script was generated Tue Dec  2 17:10:55 2014 by:
#
# Program: /tools/cds/EXT_10.12HF123/tools/extraction/bin/32bit//RCXspice
# Version: 9.1.0
# Created: Mon August 12 16:43:23 EST 2009
#
#/tools/cds/EXT_10.12HF123/tools/extraction/bin/32bit//RCXspice -techdir \
#	/tools/DesignKits/UMC/180/bu/RuleDecks/Assura/LPE/ -newlvs \
#	/home/student/us0705/projekt_IC/.LVS/LVS.xcn -assura_run_dir \
#	/home/student/us0705/projekt_IC/.LVS -assura_run_name LVS -rcxdir \
#	/home/student/us0705/projekt_IC/.LVS/LVS -xy_coordinates c,r -type \
#	full -tempdir /home/student/us0705/projekt_IC/.LVS/LVS/rcx_temp \
#	-sub_node_char # -res_models no -parasitic_res_models comment \
#	-parasitic_cap_models no -output_net_name_space layout -output \
#	/home/student/us0705/projekt_IC/.LVS/LVS/extview.tmp -net_name_space \
#	layout -macro_cell -lvs_source assura \
#	-ignore_gate_diffusion_fringing_cap -hierarchy_delimiter / -extract \
#	cap -df2 -cap_models no -cap_ground gnd! -cap_extract_mode decoupled \
#	-cap_coupling_factor 1.0
set -e
set -v
##=======================================================
##ADD_EXPLICIT_VIAS=N
##ADD_BULK_TERMINAL=N
##AGDS_FILE=/dev/null
##AGDS_LAYER_MAP_FILE=/dev/null
##HCCI_DEV_PROP_FILE=/dev/null
##AGDS_SPICE_FILE=/dev/null
##AGDS_TEXT_LAYERS=
##ARRAY_VIAS_SPACING=
##ASSURA_RUN_DIR=/home/student/us0705/projekt_IC/.LVS
##ASSURA_RUN_NAME=LVS
##BLACK_BOX_CELLS=/dev/null
##BREAK_WIDTH=
##CAP_COUPLING_FACTOR=1.0
##CAP_EXTRACT_MODE=decoupled
##CAP_GROUND=gnd!
##CAP_MODELS=no
##DANGLINGR=N
##DEVICE_FINGER_DELIMITER='@'
##DF2=Y
##DRACULA_RUN_DIR=
##DRACULA_RUN_NAME=
##ENABLESENSITIVITYEXTRACTION=N
##EXCLUDE_FLOAT_LIMIT=
##EXCLUDE_FLOAT_DECOPULING_FACTOR=
##EXCLUDE_FLOATING_NETS=N
##EXCLUDE_NETS_REDUCERC=/dev/null
##EXCLUDE_SELF_CAPS=N
##IGNORE_GATE_DIFFUSION_FRINGING_CAP=Y
##EXTRACT=cap
##EXTRACT_MOS_DIFFUSION_AP=N
##EXTRACT_MOS_DIFFUSION_HIGH=
##EXTRACT_MOS_DIFFUSION_RES=N
##FILTER_SIZE=2.0
##FIXED_NETS_FILE=/dev/null
##FMAX=
##FRACTURE_LENGTH_UNITS=microns
##FREQUENCY_FILE=/dev/null
##GROUND_NETS=
##GROUND_NETS_FILE=/dev/null
##HCCI_DEV_PROP=7
##HCCI_INST_PROP=6
##HCCI_NET_PROP=5
##HCCI_RULE_FILE=
##HCCI_RUN_DIR=
##HCCI_RUN_NAME=
##HEADER_FILE=/dev/null
##HIERARCHY_DELIMITER='/'
##HRCX_CELLS_FILE=/dev/null
##IMPORT_GLOBALS=Y
##LADDER_NETWORK=N
##LVS_SOURCE=assura
##M_FACTORR=
##M_FACTORW=N
##MACRO_CELL=Y
##MAX_FRACTURE_LENGTH=infinite
##MAX_SIGNALS=
##MERGE_PARALLEL_R=N
##MINC=
##MINC_BY_PERCENTAGE=
##MINR=0.001
##NET_NAME_SPACE=layout
##NETS_FILE=/dev/null
##OUTPUT=/home/student/us0705/projekt_IC/.LVS/LVS/extview.tmp
##OUTPUT_NET_NAME_SPACE=layout
##PARASITIC_BLOCKING_DEVICE_CELLS_TYPEgray
##PARASITIC_CAP_MODELS=no
##PARASITIC_RES_MODELS=comment
##PARASITIC_RES_LENGTH=N
##PARASITIC_RES_WIDTH=N
##PARASITIC_RES_WIDTH_DRAWN=N
##PARASITIC_RES_UNIT=N
##PARTIAL_CAP_BLOCKING=N
##PEEC=N
##PIN_ORDER_FILE=/dev/null
##PIPE_ADVGEN=
##PIPE_SPICE2DB=
##POWER_NETS=
##POWER_NETS_FILE=/dev/null
##RC_FREQUENCY=
##RCXDIR=/home/student/us0705/projekt_IC/.LVS/LVS
##RCXFS_HIGH=N
##RCXFS_NETS_FILE=/dev/null
##RCXFS_TYPE=none
##RCXFS_CUTOFF_DISTANCE=
##RCXFS_CUTOFF_DISTANCE=
##RCXFS_CUTOFF_DISTANCE=
##RCXFS_CUTOFF_DISTANCE=
##RCXFS_CUTOFF_DISTANCE=
##RCXFS_VIA_OFF=N
##REDUCERC=N
##REGION_LIMIT=
##RES_MODELS=no
##RISE_TIME=
##SAVE_FILL_SHAPES=N
##SINGLE_CAP_EDSPF=N
##SHOW_DIODES=N
##SKIN_FREQUENCY=
##SPEF=N
##SPEF_UNITS=
##SPLIT_PINS=N
##SPLIT_PINS_DISTANCE=
##SUB_NODE_CHAR='#'
##SUBSTRATE_PROFILE=/dev/null
##SUBSTRATE_STAMPING_OFF=N
##TEMPDIR=/home/student/us0705/projekt_IC/.LVS/LVS/rcx_temp
##TYPE=full
##USER_REGION=/dev/null
##VARIANT_CELL_FILE=/dev/null
##VIA_EFFECT_OFF=N
##VIRTUAL_FILL=
##XREF=/dev/null,/dev/null
##XY_COORDINATES=c,r
##=======================================================

CASE_SENSITIVE=TRUE
export CASE_SENSITIVE
TEMPDIR=`setTempDir /home/student/us0705/projekt_IC/.LVS/LVS/rcx_temp`
export TEMPDIR
DEVICE_FINGER_DELIMITER='@'
HIERARCHY_DELIMITER='/'
cd /home/student/us0705/projekt_IC/.LVS/LVS
cat <<ENDCAT> caps2dversion
* caps2d version: 10
ENDCAT
cat <<ENDCAT> flattransUnit.info
meters
ENDCAT
QRC=Y
export QRC
cat <<ENDCAT> topcellxcn.info
/home/student/us0705/projekt_IC/.LVS/LVS.xcn
ENDCAT

#==========================================================#
# Generate RCX input data from Assura LVS database
#==========================================================#

GOALIE2DIR=/tools/cds/EXT_10.12HF123/tools/extraction/bin
export GOALIE2DIR
vdbToRcx /home/student/us0705/projekt_IC/.LVS LVS -unit meters -- -V1 -H \
	satfile -r /home/student/us0705/projekt_IC/.LVS/LVS.xcn -df2 -xgl
GOALIE2DIR=/tools/cds/EXT_10.12HF123/tools/extraction/bin/32bit/
export GOALIE2DIR
geom ngate_mm_MOS_27 nsd - ngate_mm_MOS_27,10,i,1
geom pgate_mm_MOS_28 psd - pgate_mm_MOS_28,10,i,1

#==========================================================#
# Generate power list
#==========================================================#

cat global.net > power_list

#==========================================================#
# Prepare via effect layers
#==========================================================#

geom -V nsdcon M1 nsd - nsdcon_M1_nsd,111,i,1
geom -V nsdcon M1 ntap - nsdcon_M1_ntap,111,i,1
geom -V psdcon M1 psd - psdcon_M1_psd,111,i,1
geom -V psdcon M1 ptap - psdcon_M1_ptap,111,i,1

#==========================================================#
# Flatten net file, routing, via and device layers
#==========================================================#

SAVEDIR=`beginFlattenInputs`
export SAVEDIR
/bin/mv -f NET h_NET
flatnet -V -li -h '/' h_NET NET
netprint -V -N1 power_list:power_list_nums NET
flattenTransistorData ngate_mm_MOS_27 meters
flattenTransistorData pgate_mm_MOS_28 meters
flattenLayers -m M2 M1 ply nsd psd ntap ptap psub wel VI1 polycon nsdcon \
	psdcon nsdcon_M1_nsd nsdcon_M1_ntap psdcon_M1_psd psdcon_M1_ptap
endFlattenInputs

#==========================================================#
# Initialize CAP_GROUND variable
#==========================================================#

CAP_GROUND=`findCapGround -g gnd! NET`
echo "CAP_GROUND=" ${CAP_GROUND}
export CAP_GROUND

#==========================================================#
# Form capacitance layers for resistive process layers
#==========================================================#

/bin/cp ply PLY_C
/bin/cp M1 ME1_C
/bin/cp M2 ME2_C

#==========================================================#
# Form capacitance layers for non-resistive process layers
#==========================================================#

grow -V .001 nsd mask
geom -V psd mask - psd,10,i,1
grow -V .001 psd g_psd
geom -V mask,g_psd - mask,1
geom -V ntap mask - ntap,10,i,1
grow -V .001 ntap g_ntap
geom -V mask,g_ntap - mask,1
geom -V ptap mask - ptap,10,i,1
geom -V nsd,psd,ntap,ptap - PSD_C,1,i,1
createEmptyLayer ME6_C
createEmptyLayer MMCTP_C
createEmptyLayer ME5_C
createEmptyLayer ME4_C
createEmptyLayer ME3_C

#==========================================================#
# Form substrate
#==========================================================#

/bin/cp -f wel wel.df2
xytoebbox -V -g 20.002 -e ME6_C,MMCTP_C,ME5_C,ME4_C,ME3_C,ME2_C,ME1_C,PLY_C,PSD_C,psub,wel xg_wel
grow -V 0.001 wel g_wel
geom -V xg_wel g_wel - tmp_wel,10
epick -V -reo -D ${CAP_GROUND} tmp_wel pick_wel
grow -V -m 0.002 wel g_wel
stamp -i g_wel pick_wel
emerge -V pick_wel wel tmp1_wel
geom -V tmp1_wel - wel,1,i,1
/bin/rm -f g_wel xg_wel tmp_wel tmp1_wel pick_wel
grow -V 0.001 psub g_psub
geom -V wel g_psub - wel,10,i,1
geom -V psub,wel - DIFF_diel,1,i,1
geom -V DIFF_diel PSD_C - DIFF_diel,10,i,1

#==========================================================#
# Compensate for via capacitance effects
#==========================================================#

geom -V PLY_C,nsdcon_M1_nsd,nsdcon_M1_ntap,psdcon_M1_psd,psdcon_M1_ptap - PLY_C,1,i,1
geom ngate_mm_MOS_27,pgate_mm_MOS_28 - qrcgate,1,i,1

#==========================================================#
# Create sip/sw3d/cn3d capacitance data files
#==========================================================#

cat <<ENDCAT> sip.cmd
sip -V -cgnd ${CAP_GROUND} -s -o -sub 2 -mlc ME1_C,ME2_C -n 4.2 -i 0,4.201 -b \
	ME2_C,ME1_C,PLY_C,PSD_C,DIFF_diel -t ME4_C,ME5_C,MMCTP_C,ME6_C -j \
	0.28 -Maxw 4.2 -p ME3_C,key 0,4.2 - ME3_C.sip
sip -V -cgnd ${CAP_GROUND} -s -o -sub 2 -mlc ME2_C,ME3_C -n 4.2 -i 0,4.201 -b \
	ME3_C,ME2_C,ME1_C,PLY_C,PSD_C,DIFF_diel -t ME5_C,MMCTP_C,ME6_C -j \
	0.28 -Maxw 4.2 -p ME4_C,key 0,4.2 - ME4_C.sip
sip -V -cgnd ${CAP_GROUND} -s -o -sub 2 -mlc ME3_C,ME4_C -n 7 -i 0,7.001 -b \
	ME4_C,ME3_C,ME2_C,ME1_C,PLY_C,PSD_C,DIFF_diel -t MMCTP_C,ME6_C -j \
	0.28 -Maxw 4.2 -p ME5_C,key 0,7 - ME5_C.sip
sip -V -cgnd ${CAP_GROUND} -s -o -sub 2 -mlc ME4_C,ME5_C -n 5.5 -i 0,5.501 -b \
	ME5_C,ME4_C,ME3_C,ME2_C,ME1_C,PLY_C,PSD_C,DIFF_diel -t ME6_C -j 0.6 \
	-Maxw 9 -p MMCTP_C,key 0,5.5 - MMCTP_C.sip
sip -V -cgnd ${CAP_GROUND} -s -o -sub 2 -mlc ME5_C,MMCTP_C -n 10 -i 0,10.001 \
	-b MMCTP_C,ME5_C,ME4_C,ME3_C,ME2_C,ME1_C,PLY_C,PSD_C,DIFF_diel -j 1.2 \
	-Maxw 18 -p ME6_C,key 0,10 - ME6_C.sip
sip -V -cgnd ${CAP_GROUND} -s -o -sub 2 -cp PLY_C,Allgates,PSD_C -n 2.4 -i \
	0,2.401 -b PSD_C,DIFF_diel -t \
	ME1_C,ME2_C,ME3_C,ME4_C,ME5_C,MMCTP_C,ME6_C -j 0.18 -Maxw 2.7 -p \
	PLY_C,key 0,2.4 - PLY_C.sip
sip -V -cgnd ${CAP_GROUND} -s -o -sub 2 -mlc PLY_C -n 2.4 -i 0,2.401 -b \
	PLY_C,PSD_C,DIFF_diel -t ME2_C,ME3_C,ME4_C,ME5_C,MMCTP_C,ME6_C -j \
	0.24 -Maxw 3.6 -p ME1_C,key 0,2.4 - ME1_C.sip
sip -V -cgnd ${CAP_GROUND} -s -o -sub 2 -mlc PLY_C,ME1_C -n 2.8 -i 0,2.801 -b \
	ME1_C,PLY_C,PSD_C,DIFF_diel -t ME3_C,ME4_C,ME5_C,MMCTP_C,ME6_C -j \
	0.28 -Maxw 4.2 -p ME2_C,key 0,2.8 - ME2_C.sip
sip -V -s -cgnd ${CAP_GROUND} -sub 2 -L3A -h -b \
	ME5_C,ME4_C,ME3_C,ME2_C,ME1_C,PLY_C,PSD_C,DIFF_diel -Maxw 18 -p \
	MMCTP_C,key,ME6_C,key 0,10,0 - MMCTP_C_ME6_C.sip
sip -V -s -cgnd ${CAP_GROUND} -sub 2 -L3A -h -R ME6_C -b \
	ME4_C,ME3_C,ME2_C,ME1_C,PLY_C,PSD_C,DIFF_diel -Maxw 18 -p \
	ME5_C,key,ME6_C,key 0,10,0 - ME5_C_ME6_C.sip
sip -V -s -cgnd ${CAP_GROUND} -sub 2 -h -b \
	ME4_C,ME3_C,ME2_C,ME1_C,PLY_C,PSD_C,DIFF_diel -t ME6_C -Maxw 9 -p \
	ME5_C,key,MMCTP_C,key 0,7,0 - ME5_C_MMCTP_C.sip
sip -V -s -cgnd ${CAP_GROUND} -sub 2 -L3A -h -R MMCTP_C -b \
	ME3_C,ME2_C,ME1_C,PLY_C,PSD_C,DIFF_diel -t ME6_C -Maxw 9 -p \
	ME4_C,key,MMCTP_C,key 0,5.5,0 - ME4_C_MMCTP_C.sip
sip -V -s -cgnd ${CAP_GROUND} -sub 2 -h -b \
	ME3_C,ME2_C,ME1_C,PLY_C,PSD_C,DIFF_diel -t MMCTP_C,ME6_C -Maxw 4.2 -p \
	ME4_C,key,ME5_C,key 0,7,0 - ME4_C_ME5_C.sip
sip -V -s -cgnd ${CAP_GROUND} -sub 2 -L3A -h -R ME5_C -b \
	ME2_C,ME1_C,PLY_C,PSD_C,DIFF_diel -t MMCTP_C,ME6_C -Maxw 4.2 -p \
	ME3_C,key,ME5_C,key 0,7,0 - ME3_C_ME5_C.sip
sip -V -s -cgnd ${CAP_GROUND} -sub 2 -h -b ME2_C,ME1_C,PLY_C,PSD_C,DIFF_diel \
	-t ME5_C,MMCTP_C,ME6_C -Maxw 4.2 -p ME3_C,key,ME4_C,key 0,4.2,0 - \
	ME3_C_ME4_C.sip
sip -V -s -cgnd ${CAP_GROUND} -sub 2 -L3A -h -R ME4_C -b \
	ME1_C,PLY_C,PSD_C,DIFF_diel -t ME5_C,MMCTP_C,ME6_C -Maxw 4.2 -p \
	ME2_C,key,ME4_C,key 0,4.2,0 - ME2_C_ME4_C.sip
sip -V -s -cgnd ${CAP_GROUND} -sub 2 -h -b ME1_C,PLY_C,PSD_C,DIFF_diel -t \
	ME4_C,ME5_C,MMCTP_C,ME6_C -Maxw 4.2 -p ME2_C,key,ME3_C,key 0,4.2,0 - \
	ME2_C_ME3_C.sip
sip -V -s -cgnd ${CAP_GROUND} -sub 2 -L3A -h -R ME3_C -b \
	PLY_C,PSD_C,DIFF_diel -t ME4_C,ME5_C,MMCTP_C,ME6_C -k ME2_C:0.58 \
	-Maxw 4.2 -p ME1_C,key,ME3_C,key 0,4.2,0 - ME1_C_ME3_C.sip
sip -V -s -cgnd ${CAP_GROUND} -sub 2 -h -b PLY_C,PSD_C,DIFF_diel -t \
	ME3_C,ME4_C,ME5_C,MMCTP_C,ME6_C -Maxw 4.2 -p ME1_C,key,ME2_C,key \
	0,2.8,0 - ME1_C_ME2_C.sip
sip -V -s -cgnd ${CAP_GROUND} -sub 2 -L3A -h -R ME2_C -b PSD_C,DIFF_diel -t \
	ME3_C,ME4_C,ME5_C,MMCTP_C,ME6_C -k ME1_C:0.48 -Maxw 4.2 -p \
	PLY_C,key,ME2_C,key 0,2.8,0 - PLY_C_ME2_C.sip
sip -V -s -cgnd ${CAP_GROUND} -sub 2 -h -R ME1_C,PLY_C -b PSD_C,DIFF_diel -t \
	ME2_C,ME3_C,ME4_C,ME5_C,MMCTP_C,ME6_C -Maxw 3.6 -p \
	PLY_C,key,ME1_C,key 0,2.4,0 - PLY_C_ME1_C.sip
sw3d -V -cgnd ${CAP_GROUND} -sub 2 -b \
	ME5_C,ME4_C,ME3_C,ME2_C,ME1_C,PLY_C,PSD_C,DIFF_diel -p MMCTP_C,ME6_C \
	- MMCTP_C_ME6_C.sw3d
sw3d -V -cgnd ${CAP_GROUND} -sub 2 -b \
	ME4_C,ME3_C,ME2_C,ME1_C,PLY_C,PSD_C,DIFF_diel -t ME6_C -p \
	ME5_C,MMCTP_C - ME5_C_MMCTP_C.sw3d
sw3d -V -cgnd ${CAP_GROUND} -sub 2 -b ME3_C,ME2_C,ME1_C,PLY_C,PSD_C,DIFF_diel \
	-t MMCTP_C,ME6_C -p ME4_C,ME5_C - ME4_C_ME5_C.sw3d
sw3d -V -cgnd ${CAP_GROUND} -sub 2 -b ME2_C,ME1_C,PLY_C,PSD_C,DIFF_diel -t \
	ME5_C,MMCTP_C,ME6_C -p ME3_C,ME4_C - ME3_C_ME4_C.sw3d
sw3d -V -cgnd ${CAP_GROUND} -sub 2 -b ME1_C,PLY_C,PSD_C,DIFF_diel -t \
	ME4_C,ME5_C,MMCTP_C,ME6_C -p ME2_C,ME3_C - ME2_C_ME3_C.sw3d
sw3d -V -cgnd ${CAP_GROUND} -sub 2 -b PLY_C,PSD_C,DIFF_diel -t \
	ME3_C,ME4_C,ME5_C,MMCTP_C,ME6_C -p ME1_C,ME2_C - ME1_C_ME2_C.sw3d
sw3d -V -cgnd ${CAP_GROUND} -sub 2 -b PSD_C,DIFF_diel -t \
	ME2_C,ME3_C,ME4_C,ME5_C,MMCTP_C,ME6_C -p PLY_C,ME1_C - \
	PLY_C_ME1_C.sw3d
ENDCAT

#==========================================================#
# Prepare gate capacitance blocking layers
#==========================================================#

emerge -V ngate_mm_MOS_27 pgate_mm_MOS_28 Allgates

#==========================================================#
# Run pax16 to generate capfile
#==========================================================#

pax16 -V -ignore_cf_table -add_via_effect PLY_C:0.24 -scf sip.cmd -cgnd \
	${CAP_GROUND},1.0 -M_perim_off -c \
	/tools/DesignKits/UMC/180/bu/RuleDecks/Assura/LPE/cap_coeff.dat -f \
	DIFF_diel PSD_C PLY_C ME1_C ME2_C ME3_C ME4_C ME5_C MMCTP_C ME6_C \
	Allgates - \
	/tools/DesignKits/UMC/180/bu/RuleDecks/Assura/LPE/paxfile_coeff - - \
	NET - capfile

#==========================================================#
# Generate netlister data files
#==========================================================#

cat <<ENDCAT> lvsmos.mod
xN_18_MM,	100000.0, 0,	xN_18_MM,	unused, unused, 100000.0
N_18_MM,	100000.0, 0,	N_18_MM,	unused, unused, 100000.0
xP_18_MM,	100000.0, 0,	xP_18_MM,	unused, unused, 100000.0
P_18_MM,	100000.0, 0,	P_18_MM,	unused, unused, 100000.0
ENDCAT

#==========================================================#
# Process text layers
#==========================================================#

flatlabel -V  -tc -F po_textt,m1_textt,m2_textt L1T0,L2T0,L3T0

#==========================================================#
# Perform RC reduction
#==========================================================#

xreduce -V -mergecap -n NET -d1 -e \
	ME6_C,MMCTP_C,ME5_C,ME4_C,ME3_C,ME2_C,ME1_C,PLY_C,PSD_C,DIFF_diel \
	-decoupled -sr -danglingR -minR 0.001 -cap capfile \
	ngate_mm_MOS_27.trans pgate_mm_MOS_28.trans L1T0 L2T0 L3T0

#==========================================================#
# Generate HSPICE file
#==========================================================#

advgen -V -g0 -li -f -n -o HSPICE -cgnd ${CAP_GROUND},1.0 -sc caps2dversion \
	-mx capfile \
	ME6_C,MMCTP_C,ME5_C,ME4_C,ME3_C,ME2_C,ME1_C,PLY_C,PSD_C,DIFF_diel -ta \
	lvsmos.mod,ngate_mm_MOS_27.net ngate_mm_MOS_27.trans -ta \
	lvsmos.mod,pgate_mm_MOS_28.net pgate_mm_MOS_28.trans - NET - \
	/home/student/us0705/projekt_IC/.LVS/LVS/extview.tmp

#==========================================================#
# Create _save_layers file for Assura extracted view
#==========================================================#

cat <<ENDCAT> _save_layers
DIFF_diel wel psub
ME3_C ME3_C
ME4_C ME4_C
ME5_C ME5_C
MMCTP_C MMCTP_C
ME6_C ME6_C
PSD_C ptap ntap psd nsd
M2 M2
M1 M1
ply ply
nsd nsd
psd psd
ntap ntap
ptap ptap
psub psub
wel wel.df2
VI1 VI1
polycon polycon
nsdcon nsdcon_M1_ntap nsdcon_M1_nsd
psdcon psdcon_M1_ptap psdcon_M1_psd
psub_ptap_ovia psub_ptap_ovia
wel_ntap_ovia wel_ntap_ovia
ENDCAT

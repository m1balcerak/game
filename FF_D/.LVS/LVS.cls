*******************************************************************************
****** flibD schematic PRZERZUTNIKD  <vs>  flibD layout PRZERZUTNIKD
*******************************************************************************

Filter/Reduce statistics only. Network matching was OK.
                                                                                                                                                                   
Pre-expand Statistics                      
======================                          Original       
Cell/Device                               schematic  layout
(inv schematic PRZERZUTNIKD, _) Cell              4       0*
(transmisyjna schematic PRZERZUTN...) Cell        4       0*
(N_18_MM) MOS                                     0       8*
(P_18_MM) MOS                                     0       8*
                                             ------  ------
Total                                             8      16

Filter Statistics
=================                               Original            Filtered
Cell/Device                               schematic  layout   schematic  layout
(N_18_MM) MOS                                     8       8           8       8
(P_18_MM) MOS                                     8       8           8       8

Reduce Statistics
=================                               Filtered             Reduced
Cell/Device                               schematic  layout   schematic  layout
(N_18_MM) MOS                                     8       8           8       8
(P_18_MM) MOS                                     8       8           8       8
                                             ------  ------      ------  ------
Total                                            16      16          16      16

Schematic and Layout Match

*******************************************************************************
****** NAND_2 schematic NAND  <vs>  NAND_2 layout NAND
*******************************************************************************

Filter/Reduce statistics only. Network matching was OK.
                                                                                                                                                                   
Pre-expand Statistics                      
======================                          Original       
Cell/Device                               schematic  layout
(P_18_MM) MOS                                     2       2
(N_18_MM) MOS                                     2       2
                                             ------  ------
Total                                             4       4

Filter Statistics
=================                               Original            Filtered
Cell/Device                               schematic  layout   schematic  layout
(N_18_MM) MOS                                     2       2           2       2
(P_18_MM) MOS                                     2       2           2       2

Reduce Statistics
=================                               Filtered             Reduced
Cell/Device                               schematic  layout   schematic  layout
(N_18_MM) MOS                                     2       2           0       0
(P_18_MM) MOS                                     2       2           0       0
(N_18_MM:SerMos2#1) MosBlk                        -       -           1       1
(P_18_MM:ParMos2#1) MosBlk                        -       -           1       1
                                             ------  ------      ------  ------
Total                                             4       4           2       2

Schematic and Layout Match

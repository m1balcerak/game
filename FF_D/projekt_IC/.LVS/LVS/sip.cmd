sip -V -cgnd 2 -s -o -sub 2 -mlc PLY_C,ME1_C -n 2.8 -i 0,2.801 -b 	ME1_C,PLY_C,PSD_C,DIFF_diel -t ME3_C,ME4_C,ME5_C,MMCTP_C,ME6_C -j 	0.28 -Maxw 4.2 -p ME2_C,key 0,2.8 - ME2_C.sip
sip -V -cgnd 2 -s -o -sub 2 -mlc ME1_C,ME2_C -n 4.2 -i 0,4.201 -b 	ME2_C,ME1_C,PLY_C,PSD_C,DIFF_diel -t ME4_C,ME5_C,MMCTP_C,ME6_C -j 	0.28 -Maxw 4.2 -p ME3_C,key 0,4.2 - ME3_C.sip
sip -V -cgnd 2 -s -o -sub 2 -mlc ME2_C,ME3_C -n 4.2 -i 0,4.201 -b 	ME3_C,ME2_C,ME1_C,PLY_C,PSD_C,DIFF_diel -t ME5_C,MMCTP_C,ME6_C -j 	0.28 -Maxw 4.2 -p ME4_C,key 0,4.2 - ME4_C.sip
sip -V -cgnd 2 -s -o -sub 2 -mlc ME3_C,ME4_C -n 7 -i 0,7.001 -b 	ME4_C,ME3_C,ME2_C,ME1_C,PLY_C,PSD_C,DIFF_diel -t MMCTP_C,ME6_C -j 	0.28 -Maxw 4.2 -p ME5_C,key 0,7 - ME5_C.sip
sip -V -cgnd 2 -s -o -sub 2 -mlc ME4_C,ME5_C -n 5.5 -i 0,5.501 -b 	ME5_C,ME4_C,ME3_C,ME2_C,ME1_C,PLY_C,PSD_C,DIFF_diel -t ME6_C -j 0.6 	-Maxw 9 -p MMCTP_C,key 0,5.5 - MMCTP_C.sip
sip -V -cgnd 2 -s -o -sub 2 -mlc ME5_C,MMCTP_C -n 10 -i 0,10.001 	-b MMCTP_C,ME5_C,ME4_C,ME3_C,ME2_C,ME1_C,PLY_C,PSD_C,DIFF_diel -j 1.2 	-Maxw 18 -p ME6_C,key 0,10 - ME6_C.sip
sip -V -cgnd 2 -s -o -sub 2 -cp PLY_C,Allgates,PSD_C -n 2.4 -i 	0,2.401 -b PSD_C,DIFF_diel -t 	ME1_C,ME2_C,ME3_C,ME4_C,ME5_C,MMCTP_C,ME6_C -j 0.18 -Maxw 2.7 -p 	PLY_C,key 0,2.4 - PLY_C.sip
sip -V -cgnd 2 -s -o -sub 2 -mlc PLY_C -n 2.4 -i 0,2.401 -b 	PLY_C,PSD_C,DIFF_diel -t ME2_C,ME3_C,ME4_C,ME5_C,MMCTP_C,ME6_C -j 	0.24 -Maxw 3.6 -p ME1_C,key 0,2.4 - ME1_C.sip
sip -V -s -cgnd 2 -sub 2 -L3A -h -b 	ME5_C,ME4_C,ME3_C,ME2_C,ME1_C,PLY_C,PSD_C,DIFF_diel -Maxw 18 -p 	MMCTP_C,key,ME6_C,key 0,10,0 - MMCTP_C_ME6_C.sip
sip -V -s -cgnd 2 -sub 2 -L3A -h -R ME6_C -b 	ME4_C,ME3_C,ME2_C,ME1_C,PLY_C,PSD_C,DIFF_diel -Maxw 18 -p 	ME5_C,key,ME6_C,key 0,10,0 - ME5_C_ME6_C.sip
sip -V -s -cgnd 2 -sub 2 -h -b 	ME4_C,ME3_C,ME2_C,ME1_C,PLY_C,PSD_C,DIFF_diel -t ME6_C -Maxw 9 -p 	ME5_C,key,MMCTP_C,key 0,7,0 - ME5_C_MMCTP_C.sip
sip -V -s -cgnd 2 -sub 2 -L3A -h -R MMCTP_C -b 	ME3_C,ME2_C,ME1_C,PLY_C,PSD_C,DIFF_diel -t ME6_C -Maxw 9 -p 	ME4_C,key,MMCTP_C,key 0,5.5,0 - ME4_C_MMCTP_C.sip
sip -V -s -cgnd 2 -sub 2 -h -b 	ME3_C,ME2_C,ME1_C,PLY_C,PSD_C,DIFF_diel -t MMCTP_C,ME6_C -Maxw 4.2 -p 	ME4_C,key,ME5_C,key 0,7,0 - ME4_C_ME5_C.sip
sip -V -s -cgnd 2 -sub 2 -L3A -h -R ME5_C -b 	ME2_C,ME1_C,PLY_C,PSD_C,DIFF_diel -t MMCTP_C,ME6_C -Maxw 4.2 -p 	ME3_C,key,ME5_C,key 0,7,0 - ME3_C_ME5_C.sip
sip -V -s -cgnd 2 -sub 2 -h -b ME2_C,ME1_C,PLY_C,PSD_C,DIFF_diel 	-t ME5_C,MMCTP_C,ME6_C -Maxw 4.2 -p ME3_C,key,ME4_C,key 0,4.2,0 - 	ME3_C_ME4_C.sip
sip -V -s -cgnd 2 -sub 2 -L3A -h -R ME4_C -b 	ME1_C,PLY_C,PSD_C,DIFF_diel -t ME5_C,MMCTP_C,ME6_C -Maxw 4.2 -p 	ME2_C,key,ME4_C,key 0,4.2,0 - ME2_C_ME4_C.sip
sip -V -s -cgnd 2 -sub 2 -h -b ME1_C,PLY_C,PSD_C,DIFF_diel -t 	ME4_C,ME5_C,MMCTP_C,ME6_C -Maxw 4.2 -p ME2_C,key,ME3_C,key 0,4.2,0 - 	ME2_C_ME3_C.sip
sip -V -s -cgnd 2 -sub 2 -L3A -h -R ME3_C -b 	PLY_C,PSD_C,DIFF_diel -t ME4_C,ME5_C,MMCTP_C,ME6_C -Maxw 4.2 -p 	ME1_C,key,ME3_C,key 0,4.2,0 - ME1_C_ME3_C.sip
sip -V -s -cgnd 2 -sub 2 -h -b PLY_C,PSD_C,DIFF_diel -t 	ME3_C,ME4_C,ME5_C,MMCTP_C,ME6_C -Maxw 4.2 -p ME1_C,key,ME2_C,key 	0,2.8,0 - ME1_C_ME2_C.sip
sip -V -s -cgnd 2 -sub 2 -L3A -h -R ME2_C -b PSD_C,DIFF_diel -t 	ME3_C,ME4_C,ME5_C,MMCTP_C,ME6_C -k ME1_C:0.48 -Maxw 4.2 -p 	PLY_C,key,ME2_C,key 0,2.8,0 - PLY_C_ME2_C.sip
sip -V -s -cgnd 2 -sub 2 -h -R ME1_C,PLY_C -b PSD_C,DIFF_diel -t 	ME2_C,ME3_C,ME4_C,ME5_C,MMCTP_C,ME6_C -Maxw 3.6 -p 	PLY_C,key,ME1_C,key 0,2.4,0 - PLY_C_ME1_C.sip
sw3d -V -cgnd 2 -sub 2 -b 	ME5_C,ME4_C,ME3_C,ME2_C,ME1_C,PLY_C,PSD_C,DIFF_diel -p MMCTP_C,ME6_C 	- MMCTP_C_ME6_C.sw3d
sw3d -V -cgnd 2 -sub 2 -b 	ME4_C,ME3_C,ME2_C,ME1_C,PLY_C,PSD_C,DIFF_diel -t ME6_C -p 	ME5_C,MMCTP_C - ME5_C_MMCTP_C.sw3d
sw3d -V -cgnd 2 -sub 2 -b ME3_C,ME2_C,ME1_C,PLY_C,PSD_C,DIFF_diel 	-t MMCTP_C,ME6_C -p ME4_C,ME5_C - ME4_C_ME5_C.sw3d
sw3d -V -cgnd 2 -sub 2 -b ME2_C,ME1_C,PLY_C,PSD_C,DIFF_diel -t 	ME5_C,MMCTP_C,ME6_C -p ME3_C,ME4_C - ME3_C_ME4_C.sw3d
sw3d -V -cgnd 2 -sub 2 -b ME1_C,PLY_C,PSD_C,DIFF_diel -t 	ME4_C,ME5_C,MMCTP_C,ME6_C -p ME2_C,ME3_C - ME2_C_ME3_C.sw3d
sw3d -V -cgnd 2 -sub 2 -b PLY_C,PSD_C,DIFF_diel -t 	ME3_C,ME4_C,ME5_C,MMCTP_C,ME6_C -p ME1_C,ME2_C - ME1_C_ME2_C.sw3d
sw3d -V -cgnd 2 -sub 2 -b PSD_C,DIFF_diel -t 	ME2_C,ME3_C,ME4_C,ME5_C,MMCTP_C,ME6_C -p PLY_C,ME1_C - 	PLY_C_ME1_C.sw3d
